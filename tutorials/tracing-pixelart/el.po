# Dimitris Spingos (Δημήτρης Σπίγγος) <dmtrs32@gmail.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tracing Pixel Art\n"
"POT-Creation-Date: 2021-03-28 20:01+0200\n"
"PO-Revision-Date: 2019-11-14 11:38+0200\n"
"Last-Translator: Dimitris Spingos (Δημήτρης Σπίγγος) <dmtrs32@gmail.com>\n"
"Language-Team: team@lists.gnome.gr\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.0.6\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "Dimitris Spingos (Δημήτρης Σπίγγος) <dmtrs32@gmail.com>, 2014-2019"

#. (itstool) path: articleinfo/title
#: tutorial-tracing-pixelart.xml:6
msgid "Tracing Pixel Art"
msgstr "Εντοπισμός τέχνης εικονοστοιχείων"

#. (itstool) path: articleinfo/subtitle
#: tutorial-tracing-pixelart.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-tracing-pixelart.xml:11
msgid "Before we had access to great vector graphics editing software..."
msgstr ""
"Παλιότερα είχαμε πρόσβαση σε λογισμικό επεξεργασίας μεγάλων γραφικών "
"διανυσμάτων..."

#. (itstool) path: abstract/para
#: tutorial-tracing-pixelart.xml:14
msgid "Even before we had 640x480 computer displays..."
msgstr "Ακόμα πιο παλιά είχαμε οθόνες υπολογιστή 640x480..."

#. (itstool) path: abstract/para
#: tutorial-tracing-pixelart.xml:17
msgid ""
"It was common to play video games with carefully crafted pixels in low "
"resolutions displays."
msgstr ""
"Ήταν συνηθισμένο να παίζουμε παιχνίδια βίντεο με προσεκτικά φτιαγμένα "
"εικονοστοιχεία σε οθόνες χαμηλών αναλύσεων."

#. (itstool) path: abstract/para
#: tutorial-tracing-pixelart.xml:20
msgid "We name \"Pixel Art\" the kind of art born in this age."
msgstr ""
"Ονομάζουμε \"τέχνη εικονοστοιχείων (Pixel Art)\" το είδος της τέχνης που "
"γεννήθηκε αυτήν την εποχή."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:24
msgid ""
"Inkscape is powered by <ulink url=\"https://launchpad.net/libdepixelize"
"\">libdepixelize</ulink> with the ability to automatically vectorize these "
"\"special\" Pixel Art images. You can try other types of input images too, "
"but be warned: The result won't be equally good and it is a better idea to "
"use the other Inkscape tracer, potrace."
msgstr ""
"Το Inkscape τροφοδοτείται από το <ulink url=\"https://launchpad.net/"
"libdepixelize\">libdepixelize</ulink> με την ικανότητα αυτόματης "
"διανυσματοποίησης αυτών των \"ειδικών\" εικόνων τέχνης εικονοστοιχείων. "
"Μπορείτε να δοκιμάσετε άλλους τύπους εισαγωγής εικόνων επίσης, αλλά σας "
"προειδοποιούμε: Το αποτέλεσμα δεν θα είναι εξίσου καλό και είναι καλύτερη "
"ιδέα να χρησιμοποιήσετε τον άλλο ανιχνευτή του Inkscape, potrace."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:30
msgid ""
"Let's start with a sample image to show you the capabilities of this tracer "
"engine. Below there is an example of a raster image (taken from a Liberated "
"Pixel Cup entry) on the left and its vectorized output on the right."
msgstr ""
"Ας αρχίσουμε με ένα δείγμα εικόνας για να σας εμφανίσουμε τις δυνατότητες "
"αυτής της μηχανής ανίχνευσης. Παρακάτω υπάρχει ένα παράδειγμα εικονογραφίας "
"(που ελήφθη από μια καταχώριση στο Liberated Pixel Cup) στα αριστερά και την "
"διανυσματική του έξοδο στα δεξιά."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:41
msgid ""
"libdepixelize uses Kopf-Lischinski algorithm to vectorize images. This "
"algorithm uses ideas of several computer science techniques and math "
"concepts to produce a good result for pixel art images. One thing to notice "
"is that the alpha channel is completely ignored by the algorithm. "
"libdepixelize has currently no extensions to give a first-class citizen "
"treatment for this class of images, but all pixel art images with alpha "
"channel support are producing results similar to the main class of images "
"recognized by Kopf-Lischinski."
msgstr ""
"Το libdepixelize χρησιμοποιεί τον αλγόριθμο Kopf-Lischinski για να κάνει "
"διανύσματα εικόνων. Αυτός ο αλγόριθμος χρησιμοποιεί ιδέες πολλών τεχνικών "
"της επιστήμης των υπολογιστών και μαθηματικές έννοιες για να παράξει ένα "
"καλό αποτέλεσμα για εικόνες τέχνης εικονοστοιχείων. Πρέπει να σημειώσετε ότι "
"το κανάλι άλφα παραβλέπεται ολότελα από τον αλγόριθμο. Το libdepixelize δεν "
"έχει προς το παρόν επεκτάσεις για να σας δώσει μια πρώτης τάξης συμπεριφορά "
"για αυτήν την κλάση εικόνων, αλλά όλες οι εικόνες τέχνης εικονοστοιχείων "
"(pixel art) με υποστήριξη καναλιού άλφα παράγουν παρόμοια αποτελέσματα με "
"την κύρια κλάση των εικόνων που αναγνωρίζονται από τον Kopf-Lischinski."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:55
msgid ""
"The image above has alpha channel and the result is just fine. Still, if you "
"find a pixel art image with a bad result and you believe that the reason is "
"the alpha channel, then contact libdepixelize maintainer (e.g. fill a bug on "
"the project page) and he will be happy to extend the algorithm. He can't "
"extend the algorithm if he don't know what images are giving bad results."
msgstr ""
"Η παραπάνω εικόνα έχει κανάλι άλφα και το αποτέλεσμα είναι απλώς θαυμάσιο. "
"Παρόλα αυτά, αν βρείτε μια εικόνα τέχνης εικονοστοιχείων με άσχημο "
"αποτέλεσμα και πιστεύετε ότι η αιτία είναι το κανάλι άλφα, τότε "
"επικοινωνήστε με τον συντηρητή του libdepixelize (π.χ. συμπληρώστε ένα "
"σφάλμα στη σελίδα έργου) και θα είναι ευχαριστημένος να επεκτείνει τον "
"αλγόριθμο. Δεν μπορεί να επεκτείνει τον αλγόριθμο αν δεν ξέρει ποιες εικόνες "
"δίνουν άσχημα αποτελέσματα."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:61
#, fuzzy
msgid ""
"The image below is a screenshot of <guimenuitem>Pixel art</guimenuitem> "
"dialog in the English localisation. You can open this dialog using the "
"<menuchoice><guimenu>Path</guimenu><guisubmenu>Trace Bitmap</"
"guisubmenu><guimenuitem>Pixel art</guimenuitem></menuchoice> menu or right-"
"clicking on an image object and then <guimenuitem>Trace Bitmap</guimenuitem>."
msgstr ""
"Η παρακάτω εικόνα είναι ένα στιγμιότυπο του διαλόγου <command>Ανίχνευση "
"τέχνης εικονοστοιχείων</command> στην αγγλική τοπικοποίηση. Μπορείτε να "
"ανοίξετε αυτόν τον διάλογο χρησιμοποιώντας το μενού <command>Διαδρομή &gt; "
"Ανίχνευση τέχνης εικονοστοιχείων...</command> ή δεξιοπατώντας σε ένα "
"αντικείμενο εικόνας και έπειτα <command>Ανίχνευση τέχνης εικονοστοιχείων</"
"command>."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:74
msgid ""
"This dialog has two sections: Heuristics and output. Heuristics is targeted "
"at advanced uses, but there already good defaults and you shouldn't worry "
"about that, so let's leave it for later and starting with the explanation "
"for output."
msgstr ""
"Αυτός ο διάλογος έχει δύο ενότητες: Ευρετική και έξοδος. Η ευρετική "
"αποσκοπεί σε προχωρημένες χρήσεις, αλλά υπάρχουν ήδη καλές προεπιλογές και "
"δεν πρέπει να ανησυχείτε για αυτό, έτσι ας το αφήσουμε για αργότερα και να "
"ξεκινήσουμε με την εξήγηση της εξόδου."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:79
msgid ""
"Kopf-Lischinski algorithm works (from a high-level point of view) like a "
"compiler, converting the data among several types of representation. At each "
"step the algorithm has the opportunity to explore the operations that this "
"representation offers. Some of these intermediate representations have a "
"correct visual representation (like the reshaped cell graph Voronoi output) "
"and some don't (like the similarity graph). During development of "
"libdepixelize users kept asking for adding the possibility of export these "
"intermediate stages to the libdepixelize and the original libdepixelize "
"author granted their wishes."
msgstr ""
"Ο αλγόριθμος Kopf-Lischinski δουλεύει (από ένα σημείο υψηλού επιπέδου "
"προβολής) όπως ένας μεταγλωττιστής, στη μετατροπή δεδομένων μεταξύ πολλών "
"τύπων παρουσίασης. Σε κάθε βήμα ο αλγόριθμος έχει την ευκαιρία να "
"εξερευνήσει τις λειτουργίες που προσφέρει αυτή η απεικόνιση. Κάποιες από "
"αυτές τις ενδιάμεσες αναπαραστάσεις έχουν μια σωστή οπτική παρουσίαση (όπως "
"η ανασχεδιασμένη έξοδος Voronoi του γραφήματος κελιού) και κάποιες δεν έχουν "
"(όπως το γράφημα ομοιότητας). Κατά την ανάπτυξη του libdepixelize οι χρήστες "
"συνέχισαν να ρωτούν για την προσθήκη της δυνατότητας εξαγωγής αυτών των "
"ενδιάμεσων σταδίων στο libdepixelize και ο αρχικός συγγραφέας του "
"libdepixelize δέχτηκε τις επιθυμίες τους."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:87
#, fuzzy
msgid ""
"The default output should give the smoothest result and is probably what you "
"want. You saw already the default output on the first samples of this "
"tutorial. If you want to try it yourself, just open the <guimenuitem>Trace "
"Bitmap</guimenuitem> dialog, select <guimenuitem>Pixel art</guimenuitem> tab "
"and click in <guibutton>OK</guibutton> after choosing some image on Inkscape."
msgstr ""
"Η προεπιλεγμένη έξοδος πρέπει να δίνει το πιο ομαλό αποτέλεσμα και είναι "
"προφανώς αυτή που θέλετε. Είδατε ήδη την προεπιλεγμένη έξοδο στα πρώτα "
"δείγματα αυτού του μαθήματος. Αν θέλετε να το δοκιμάσετε οι ίδιοι, ανοίξτε "
"απλά τον διάλογο <command>Ανίχνευση τέχνης εικονοστοιχείων</command> και "
"πατήστε στο <command>Εντάξει</command> μετά την επιλογή κάποιας εικόνας στο "
"Inkscape."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:93
msgid ""
"You can see the Voronoi output below and this is a \"reshaped pixel image\", "
"where the cells (previously pixels) got reshaped to connect pixels that are "
"part of the same feature. No curves will be created and the image continues "
"to be composed of straight lines. The difference can be observed when you "
"magnify the image. Previously pixels couldn't share a edge with a diagonal "
"neighbour, even if it was meant to be part of the same feature. But now "
"(thanks to a color similarity graph and the heuristics that you can tune to "
"achieve a better result), it's possible to make two diagonal cells share an "
"edge (previously only single vertices were shared by two diagonal cells)."
msgstr ""
"Μπορείτε να δείτε την παρακάτω έξοδο Voronoi και αυτή είναι μια "
"\"ανασχεδιασμένη εικόνα εικονοστοιχείων\", όπου τα κελιά (προηγουμένως "
"εικονοστοιχεία) ανασχεδιάστηκαν για να συνδέσουν εικονοστοιχεία που είναι "
"μέρη του ίδιου γνωρίσματος. Δεν θα δημιουργηθούν καμπύλες και η εικόνα "
"συνεχίζει να αποτελείται από ευθείες γραμμές. Η διαφορά μπορεί να "
"παρατηρηθεί όταν μεγεθύνετε την εικόνα. Προηγουμένως, τα εικονοστοιχεία δεν "
"μπορούσαν να μοιραστούν μια άκρη με έναν διαγώνιο γείτονα, ακόμα κι αν "
"προοριζόταν να είναι μέρος του ίδιου γνωρίσματος. Αλλά τώρα (χάρη σε ένα "
"γράφημα ομοιότητας χρώματος και την ευρετική μπορείτε να πετύχετε ένα "
"καλύτερο αποτέλεσμα), είναι δυνατό να κάνετε δύο διαγώνια κελιά να "
"μοιραστούν μια άκρη (προηγουμένως μόνο μεμονωμένες κορυφές μοιραζόντουσαν "
"από δύο διαγώνια κελιά)."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:109
msgid ""
"The standard B-splines output will give you smooth results, because the "
"previous Voronoi output will be converted to quadratic Bézier curves. "
"However, the conversion won't be 1:1 because there are more heuristics "
"working to decide which curves will be merged into one when the algorithm "
"reaches a T-junction among the visible colors. A hint about the heuristics "
"of this stage: You can't tune them."
msgstr ""
"Η τυπική έξοδο Β-ευλύγιστων καμπυλών θα σας δώσει ομαλά αποτελέσματα, επειδή "
"η προηγούμενη έξοδος Βορονόι θα μετατραπεί σε δευτεροβάθμιες καμπύλες "
"Μπεζιέ. Όμως, η μετατροπή δεν θα είναι 1:1 επειδή υπάρχουν περισσότερα "
"ευρετικά που δουλεύουν για να αποφασίσουν ποιες καμπύλες θα συγχωνευτούν σε "
"μία όταν ο αλγόριθμος φτάσει μια ένωση Τ μεταξύ των ορατών χρωμάτων. Μια "
"υπόδειξη για την ευρετική αυτού του σταδίου: Δεν μπορείτε να την συντονίσετε."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:115
msgid ""
"The final stage of libdepixelize (currently not exportable by the Inkscape "
"GUI because of its experimental and incomplete state) is \"optimize curves\" "
"to remove the staircasing effect of the B-Spline curves. This stage also "
"performs a border detection technique to prevent some features from being "
"smoothed and a triangulation technique to fix the position of the nodes "
"after optimization. You should be able to individually disable each of these "
"features when this output leaves the \"experimental stage\" in libdepixelize "
"(hopefully soon)."
msgstr ""
"Το τελικό στάδιο του libdepixelize (προς το παρόν μη εξαγώγιμο από το GUI "
"Inkscape λόγω της πειραματικής και ατελούς κατάστασης) είναι "
"\"βελτιστοποίηση καμπυλών\" για να αφαιρέσετε το αποτέλεσμα της σκάλας των Β-"
"εύκαμπτων καμπυλών. Αυτό το στάδιο εκτελεί επίσης μια τεχνική αναγνώρισης "
"ορίου για να αποτρέψει κάποια γνωρίσματα από την εξομάλυνση και μια τεχνική "
"τριγωνισμού για να διορθώσει τη θέση των κόμβων μετά τη βελτιστοποίηση. Θα "
"πρέπει να μπορείτε να απενεργοποιήσετε ξεχωριστά κάθε ένα από αυτά τα "
"γνωρίσματα όταν αυτή η έξοδος αφήνει το \"πειραματικό στάδιο\" στο "
"libdepixelize (ελπίζουμε σύντομα)."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:122
msgid ""
"The heuristics section in the gui allows you to tune the heuristics used by "
"libdepixelize to decide what to do when it encounters a 2x2 pixel block "
"where the two diagonals have similar colors. \"What connection should I keep?"
"\" is what libdepixelize asks. It tries to apply all heuristics to the "
"conflicting diagonals and keeps the connection of the winner. If a tie "
"happens, both connections are erased."
msgstr ""
"Η ενότητα ευρετικών στη γραφική διεπαφή σας επιτρέπει να συντονίσετε τα "
"χρησιμοποιούμενα ευρετικά από το libdepixelize για να αποφασίσει τι να κάνει "
"όταν αντιμετωπίσει μια ομάδα 2x2 εικονοστοιχείων όπου οι δύο διαγώνιοι έχουν "
"παρόμοια χρώματα. \"Ποια σύνδεση πρέπει να κρατήσω;\" είναι αυτό που ρωτά το "
"libdepixelize. Προσπαθεί να εφαρμόσει όλα τα ευρετικά στις συγκρουόμενες "
"διαγωνίους και κρατά τη σύνδεση του νικητή. Αν συμβεί ισοπαλία και οι δυο "
"συνδέσεις σβήνονται."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:128
msgid ""
"If you want to analyze the effect of each heuristic and play with the "
"numbers, the best output is the Voronoi output. You can see more easily the "
"effects of the heuristics in the Voronoi output and when you are satisfied "
"with the settings you got, you can just change the output type to the one "
"you want."
msgstr ""
"Αν θέλετε να αναλύσετε το αποτέλεσμα κάθε ευρετικού και να παίξετε με τους "
"αριθμούς, η άριστη έξοδος είναι η έξοδος Βορονόι. Μπορείτε να δείτε πιο "
"εύκολα τα αποτελέσματα των ευρετικών στην έξοδο Βορονόι και όταν είσαστε "
"ικανοποιημένοι με τις ρυθμίσεις που πήρατε, μπορείτε απλώς να αλλάξετε τον "
"τύπο εξόδου στον επιθυμητό."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:133
msgid ""
"The image below has an image and the B-Splines output with only one of the "
"heuristics turned on for each try. Pay attention to the purple circles that "
"highlight the differences that each heuristic performs."
msgstr ""
"Η παρακάτω εικόνα έχει μια εικόνα και την έξοδο των ευλύγιστων καμπυλών Β με "
"μόνο μια από τις ευρετικές ενεργή για κάθε προσπάθεια. Προσέξτε τους "
"πορφυρούς κύκλους που επισημαίνουν τις διαφορές που κάθε ευρετική εκτελεί."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:144
msgid ""
"For the first try (top image), we only enable the curves heuristic. This "
"heuristic tries to keep long curves connected together. You can notice that "
"its result is similar to the last image, where the sparse pixels heuristic "
"is applied. One difference is that its \"strength\" is more fair and it only "
"gives a high value to its vote when it's really important to keep these "
"connections. The \"fair\" definition/concept here is based on \"human "
"intuition\" given the pixel database analysed. Another difference is that "
"this heuristic can't decide what to do when the connections group large "
"blocks instead of long curves (think about a chess board)."
msgstr ""
"Για την πρώτη προσπάθεια (πάνω εικόνα), ενεργοποιήσαμε μόνο τις ευρετικές "
"καμπύλες. Αυτή η ευρετική προσπαθεί να κρατήσει τις μεγάλες καμπύλες "
"συνδεμένες μαζί. Μπορείτε να σημειώστε ότι το αποτέλεσμά του είναι παρόμοιο "
"με την τελευταία εικόνα, όπου τα αραιά ευρετικά εικονοστοιχεία εφαρμόζονται. "
"Μια διαφορά είναι ότι η \"δύναμή\" του είναι περισσότερο επαρκής και δίνει "
"μόνο μια υψηλή τιμή στον ψήφο του όταν είναι πραγματικά σημαντικό να "
"κρατήσετε αυτές τις συνδέσεις. Ο \"επαρκής\" ορισμός/έννοια εδώ βασίζεται "
"στην \"ανθρώπινη διαίσθηση\" με δεδομένη την αναλυμένη βάση δεδομένων του "
"εικονοστοιχείου. Μια άλλη διαφορά είναι ότι αυτή η ευρετική δεν μπορεί να "
"αποφασίσει τι να κάνει όταν υπάρχουν μεγάλα μπλοκ ομάδας συνδέσεων αντί για "
"μεγάλες καμπύλες (σκεφτείτε μια σκακιέρα)."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:152
msgid ""
"For the second try (the middle image), we only enable the islands heuristic. "
"The only thing this heuristic does is trying to keep the connection that "
"otherwise would result in several isolated pixels (islands) with a constant "
"weight vote. This kind of situation is not as common as the kind of "
"situation handled by the other heuristics, but this heuristic is cool and "
"help to give still better results."
msgstr ""
"Για τη δεύτερη προσπάθεια (την μεσαία εικόνα), ενεργοποιούμε μόνο τις "
"ευρετικές νησίδων. Το μόνο πράγμα που αυτή η ευρετική κάνει είναι η "
"προσπάθεια διατήρησης της σύνδεσης που αλλιώς θα κατέληγε σε πολλά "
"απομονωμένα εικονοστοιχεία (νησίδες) με ένα σταθερό βάρος συμφωνίας. Αυτό το "
"είδος κατάστασης δεν είναι τόσο συνηθισμένο όσο το είδος της κατάστασης που "
"χειρίζονται οι άλλες ευρετικές, αλλά αυτή η ευρετική είναι πολύ καλή και "
"βοηθάει στην παραγωγή ακόμα καλύτερων αποτελεσμάτων."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:158
msgid ""
"For the third try (the bottom image), we only enable the sparse pixels "
"heuristic. This heuristic tries to keep the curves with the foreground color "
"connected. To find out what is the foreground color the heuristic analyzes a "
"window with the pixels around the conflicting curves. For this heuristic, "
"you not only tune its \"strength\", but also the window of pixels it "
"analyzes. But keep in mind that when you increase the window of pixels "
"analyzed the maximum \"strength\" for its vote will increase too and you "
"might want to adjust the multiplier for its vote. The original libdepixelize "
"author think this heuristic is too greedy and likes to use the \"0.25\" "
"value for its multiplier."
msgstr ""
"Για την τρίτη προσπάθεια (η κάτω εικόνα), ενεργοποιήσαμε μόνο την αραιή "
"ευρετική των εικονοστοιχείων. Αυτή η ευρετική προσπαθεί να κρατήσει τις "
"καμπύλες με το χρώμα προσκηνίου συνδεμένες. Για να βρει ποιο είναι το χρώμα "
"παρασκηνίου η ευρετική αναλύει ένα παράθυρο με τα εικονοστοιχεία γύρω από "
"τις συγκρουόμενες καμπύλες. Για αυτήν την ευρετική, δεν συντονίζετε μόνο τη "
"\"δύναμή\" της, αλλά επίσης το παράθυρο των εικονοστοιχείων που αναλύει. Να "
"θυμόσαστε ότι όταν αυξάνετε το παράθυρο των αναλυμένων εικονοστοιχείων η "
"μέγιστη \"δύναμη\" για τη συμφωνία του θα αυξηθεί επίσης και μπορεί να "
"θελήσετε να προσαρμόσετε τον πολλαπλασιαστή για την συμφωνία του. Ο αρχικός "
"συγγραφέας του libdepixelize θεωρεί ότι αυτή η ευρετική είναι υπερβολικά "
"αδηφάγος και του αρέσει να χρησιμοποιεί την τιμή \"0.25\" για τον "
"πολλαπλασιαστή του."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:167
msgid ""
"Even if the results of the curves heuristic and the sparse pixels heuristic "
"give similar results, you may want to leave both enabled, because the curves "
"heuristic may give an extra safety that the important curves of contour "
"pixels won't be hampered and there are cases that can be only answered by "
"the sparse pixels heuristic."
msgstr ""
"Ακόμα κι αν τα αποτελέσματα των ευρετικών καμπυλών και των αραιών ευρετικών "
"εικονοστοιχείων δίνουν παρόμοια αποτελέσματα, μπορεί να θελήσετε να τα "
"αφήσετε και τα δυο ενεργά, επειδή οι ευρετικές καμπύλες μπορεί να δώσουν μια "
"πρόσθετη ασφάλεια έτσι ώστε οι σημαντικές καμπύλες των εικονοστοιχείων "
"περιγράμματος δεν θα εμποδίζονται και υπάρχουν περιπτώσεις που μπορούν να "
"απαντηθούν μόνο από την αραιή ευρετική εικονοστοιχείων."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:172
msgid ""
"Hint: You can disable all heuristics by setting its multiplier/weight values "
"to zero. You can make any heuristic act against its principles using "
"negative values for its multiplier/weight values. Why would you ever want to "
"replace behaviour that was created to give better quality by the opposite "
"behaviour? Because you can... because you might want a \"artistic\" "
"result... whatever... you just can."
msgstr ""
"Υπόδειξη: Μπορείτε να απενεργοποιήσετε όλες τις ευρετικές ορίζοντας τις "
"τιμές του πολλαπλασιαστή/βάρους στο μηδέν. Μπορείτε να κάνετε οποιαδήποτε "
"ευρετική πράξη αντίθετα προς τις αρχές τους χρησιμοποιώντας αρνητικές τιμές "
"για τις τιμές πολλαπλασιαστή/βάρους. Γιατί θα θέλατε κάποτε να "
"αντικαταστήσετε τη συμπεριφορά που δημιουργήθηκε για να δώσετε καλύτερη "
"ποιότητα με την αντίθετη συμπεριφορά; Επειδή μπορείτε... επειδή μπορεί να "
"θελήσετε ένα \"καλλιτεχνικό\" αποτέλεσμα... ο,τιδήποτε απλά μπορείτε."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:178
msgid ""
"And that's it! For this initial release of libdepixelize these are all the "
"options you got. But if the research of the original libdepixelize author "
"and its creative mentor succeeds, you may receive extra options that broaden "
"even yet the range of images for which libdepixelize gives a good result. "
"Wish them luck."
msgstr ""
"Αυτό ήτανε. Για αυτήν την αρχική έκδοση του libdepixelize αυτές είναι όλες "
"οι επιλογές που έχετε. Αλλά αν η αναζήτηση του αρχικού συγγραφέα του "
"libdepixelize και των δημιουργικών συμβούλων πετύχει, μπορεί να δεχτείτε "
"πρόσθετες επιλογές που διευρύνουν ακόμα περισσότερο το εύρος των εικόνων για "
"τις οποίες το libdepixelize δίνει ένα καλό αποτέλεσμα. Να τους ευχηθούμε "
"καλή τύχη."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:183
msgid ""
"All images used here were taken from Liberated Pixel Cup to avoid copyright "
"problems. The links are:"
msgstr ""
"Όλες οι χρησιμοποιούμενες εικόνες ελήφθησαν από το Liberated Pixel Cup προς "
"αποφυγή προβλημάτων πνευματικών δικαιωμάτων. Οι σύνδεσμοι είναι:"

#. (itstool) path: listitem/para
#: tutorial-tracing-pixelart.xml:188
msgid ""
"<ulink url=\"http://opengameart.org/content/memento\">http://opengameart.org/"
"content/memento</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tracing-pixelart.xml:193
msgid ""
"<ulink url=\"http://opengameart.org/content/rpg-enemies-bathroom-tiles"
"\">http://opengameart.org/content/rpg-enemies-bathroom-tiles</ulink>"
msgstr ""

#. (itstool) path: Work/format
#: tracing-pixelart-f01.svg:49 tracing-pixelart-f02.svg:49
#: tracing-pixelart-f03.svg:49 tracing-pixelart-f04.svg:49
#: tracing-pixelart-f05.svg:49
msgid "image/svg+xml"
msgstr "image/svg+xml"

#~ msgid "http://opengameart.org/content/memento"
#~ msgstr "http://opengameart.org/content/memento"

#~ msgid "http://opengameart.org/content/rpg-enemies-bathroom-tiles"
#~ msgstr "http://opengameart.org/content/rpg-enemies-bathroom-tiles"
